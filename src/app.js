// Compile styles
require('./scss/style.scss');

import jQuery from 'jquery';
import 'slick-carousel';

window.$ = window.jQuery = jQuery;

$(document).ready(() => {
    $('#language-chooser').on('change', function() {
        window.location.href = $(this).val();
    });

    $('.slides').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '200px',
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    centerPadding: '0'
                },
            },
        ],
    });

    $('.tabs a').on('click', function() {
        var $tablink = $(this);
        var target = $tablink.attr('href');

        $tablink.parent().toggleClass('open');
        $tablink
            .addClass('active')
            .siblings()
            .removeClass('active');
        $(target)
            .addClass('active')
            .siblings('.tabcontent')
            .removeClass('active');

        return false;
    });

    $('body').on('click', function() {
        $('.tabs').removeClass('open');
    });

    $('.toggle-menu').on('click', function() {
        $('.main-menu').slideToggle({
            start: function() {
                $(this).css('display', 'flex');
            },
        });

        return false;
    });
});
