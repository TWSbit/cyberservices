const path                      = require('path');
const UglifyJsPlugin            = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin         = require('html-webpack-plugin');
const CleanWebpackPlugin        = require('clean-webpack-plugin');
const MiniCssExtractPlugin      = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin   = require('optimize-css-assets-webpack-plugin');
// const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, './src'),

    entry: {
        app: './app.js',
    },

    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'js/[name].min.js'
    },

    devtool: 'source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: ['es2015']
                },
                exclude: /node_modules/
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                loaders: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'img/[name].[ext]'
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                                progressive: true,
                                quality: 65
                            },
                            optipng: {
                                enabled: false,
                            },
                            pngquant: {
                                quality: '65-90',
                                speed: 4
                            },
                            gifsicle: {
                                interlaced: false,
                            }
                        }
                    }
                ]
            },

            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            minimize: true
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            }

        ],
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                parallel: true,
                uglifyOptions: {
                    compress: {
                        warnings: false,
                        conditionals: true,
                        unused: true,
                        comparisons: true,
                        sequences: true,
                        dead_code: true,
                        evaluate: true,
                        join_vars: false,
                        if_return: true
                    },
                    output: {
                        comments: false
                    },
                    sourceMap: true
                },
            }),
            new OptimizeCSSAssetsPlugin({
                cssProcessorPluginOptions: {
                    preset: ['default', { discardComments: { removeAll: true } }],
                },
            }),
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),

        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),

        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.ejs',
            hash: true,
            minify: {
                removeComments: true,
                collapseWhitespace: false
            }
        }),

        new HtmlWebpackPlugin({
            filename: 'details.html',
            template: 'details.ejs',
            hash: true,
            minify: {
                removeComments: true,
                collapseWhitespace: false
            }
        }),

        // new FaviconsWebpackPlugin({
        //     logo:   './img/favicon.png',
        //     title:  'CyberServices',
        //     prefix: 'img/',
        //     theme_color: "#690",
        //     icons: {
        //         android: true,
        //         appleIcon: true,
        //         appleStartup: false,
        //         coast: false,
        //         favicons: true,
        //         firefox: false,
        //         opengraph: false,
        //         twitter: false,
        //         yandex: false,
        //         windows: true
        //     }
        // })
    ],

    devServer: {
        contentBase: path.resolve(__dirname, './src'),
        compress: true,
        port: 9000
    },

    watch: true
};
